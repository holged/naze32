# NAZE32飞控

## 介绍
NAZE32飞控，烧录BetaFlight或CleanFlight固件，使用STM32F103C8T6，板载加速度计、气压计、FLASH（用于黑匣子）

![](7.图片/%E5%AE%9E%E7%89%A9.jpg)

### NAZE32飞控V1

![img](7.图片/NAZE32%E9%A3%9E%E6%8E%A7V1_3D%E9%A1%B6%E9%9D%A2.png)

![img](7.%E5%9B%BE%E7%89%87/NAZE32%E9%A3%9E%E6%8E%A7V1_3D%E5%BA%95%E9%9D%A2.png)

### NAZE32飞控V2

![img](7.图片/NAZE32%E9%A3%9E%E6%8E%A7V2_3D%E9%A1%B6%E9%9D%A2.png)

![img](7.图片/NAZE32%E9%A3%9E%E6%8E%A7V2_3D%E5%BA%95%E9%9D%A2.png)

 **个人更推荐V2版本**，相比于V1版本，V2改进内容：

1.MicroUSB接口更换为Type-C接口；

2.USB供电(约5.2V)与外部5V供电之间使用BAT54C整流；

3.电调接口由3*6P排针更换为1*8P焊盘；

4.增加通信串口1*4P焊盘及其SH1.0插座；

5.增加板上FLASH，用于黑匣子功能。 

## 教程

焊接说明、接线说明等：[博客园](https://www.cnblogs.com/cai-zi/p/15837956.html)

![](7.图片/NAZE32%E9%A3%9E%E6%8E%A7V2%E6%8E%A5%E7%BA%BF.jpg)

## BOM

### NAZE V1

| 序号 | 名称                  | 编号                                    | 封装                               | 数量 | 商品链接                                                 |
| ---- | --------------------- | --------------------------------------- | ---------------------------------- | ---- | -------------------------------------------------------- |
| 1    | 3x3x1.5MM_SMD         | BOOT1                                   | KEY-SMD_4P-L3.5-W3.4-P2.20-LS3.9   | 1    | [链接](https://item.taobao.com/item.htm?id=546724337380) |
| 2    | 0.1uF                 | C1,C5,C6,C7,C11,C12,C14,C15,C19,C20,C21 | C0603                              | 11   | [链接](https://item.taobao.com/item.htm?id=522573286298) |
| 3    | 1uF                   | C3,C4,C9,C10,C13,C18                    | C0603                              | 6    |                                                          |
| 4    | 2.2nF                 | C16                                     | C0603                              | 1    |                                                          |
| 5    | 0.01uF                | C17                                     | C0603                              | 1    |                                                          |
| 6    | 10uF                  | C2,C8                                   | C0805                              | 2    |                                                          |
| 7    | 1N5819WS              | D1                                      | SOD-323_L1.8-W1.3-LS2.5-RD         | 1    | [链接](https://item.taobao.com/item.htm?id=522574162326) |
| 8    | LED-0603_G            | LED1                                    | LED0603_GREEN                      | 1    |                                                          |
| 9    | LED-0603_B            | LED2                                    | LED0603_BLUE                       | 1    |                                                          |
| 10   | LED-0603_R            | LED3                                    | LED0603_RED                        | 1    | [链接](https://item.taobao.com/item.htm?id=522554271548) |
| 11   | SS8050                | Q1,Q2                                   | SOT-23(SOT-23-3)                   | 2    | [链接](https://item.taobao.com/item.htm?id=522573790562) |
| 12   | 10k                   | R1,R2,R4,R5,R14,R15                     | R0603                              | 6    | [链接](https://item.taobao.com/item.htm?id=525777943950) |
| 13   | 100k                  | R10                                     | R0603                              | 1    |                                                          |
| 14   | 20R                   | R16                                     | R0603                              | 1    |                                                          |
| 15   | 1k                    | R3,R6,R11,R12,R13                       | R0603                              | 5    |                                                          |
| 16   | 4K7                   | R7,R8,R9                                | R0603                              | 3    |                                                          |
| 17   | 1k                    | RN1,RN2                                 | RES-ARRAY-SMD_0603-8P-L3.2-W1.6-BL | 2    | [链接](https://item.taobao.com/item.htm?id=522554151720) |
| 18   | RT9013-33GB           | U1,U3                                   | SOT-23-5_L3.0-W1.7-P0.95-LS2.8-BR  | 2    | [链接](https://item.taobao.com/item.htm?id=546701607083) |
| 19   | STM32F103C8T6         | U2                                      | LQFP-48_L7.0-W7.0-P0.50-LS9.0-BL   | 1    |                                                          |
| 20   | BMP280                | U4                                      | SENSOR-TH_BMP280-BL                | 1    | [链接](https://item.taobao.com/item.htm?id=536588151627) |
| 21   | MPU6050               | U5                                      | QFN50P400X400X95-25T265X275N       | 1    | [链接](https://item.taobao.com/item.htm?id=522575310310) |
| 22   | CP2104-F03-GM         | U6                                      | QFN-24_L4.0-W4.0-P0.50-BL-EP2.7    | 1    | [链接](https://item.taobao.com/item.htm?id=552233398369) |
| 23   | U-F-M5DD-Y-1          | USB1                                    | MICRO-USB-SMD_U-F-M5DD-Y-1         | 1    | [链接](https://item.taobao.com/item.htm?id=538106330041) |
| 24   | 8MHz(CSTCE8M00G52-R0) | X1                                      | OSC-SMD_3P-L3.2-W1.3-P1.2-L        | 1    | [链接](https://item.taobao.com/item.htm?id=620069296989) |

### NAZE V2

| 序号 | 名称           | 编号                                    | 封装                               | 数量 | 商品链接                                         |
| ---- | -------------- | --------------------------------------- | ---------------------------------- | ---- | ------------------------------------------------ |
| 1    | 3x3x1.5MM_SMD  | BOOT1                                   | KEY-SMD_4P-L3.5-W3.4-P2.20-LS3.9   | 1    | [链接](https://item.taobao.com/item.htm?id=546724337380) |
| 2    | 0.1uF          | C1,C5,C6,C7,C11,C12,C14,C15,C19,C20,C21 | C0603                              | 11   | [链接](https://item.taobao.com/item.htm?id=522573286298) |
| 3    | 2.2nF          | C16                                     | C0603                              | 1    |                                                  |
| 4    | 0.01uF         | C17                                     | C0603                              | 1    |                                                  |
| 5    | 10uF           | C2,C8                                   | C0805                              | 2    |                                                  |
| 6    | 1uF            | C3,C4,C9,C10,C13,C18                    | C0603                              | 6    |                                                  |
| 7    | 1N5819WS       | D1                                      | SOD-323_L1.8-W1.3-LS2.5-RD         | 1    | [链接](https://item.taobao.com/item.htm?id=522574162326) |
| 8    | BAT54C         | D2                                      | SOT-23-3_L2.9-W1.6-P1.90-LS2.8-BR  | 1    | [链接](https://item.taobao.com/item.htm?id=661043868599) |
| 9    | LED-0603_G     | LED1                                    | LED0603_GREEN                      | 1    |                                                  |
| 10   | LED-0603_B     | LED2                                    | LED0603_BLUE                       | 1    |                                                  |
| 11   | LED-0603_R     | LED3                                    | LED0603_RED                        | 1    | [链接](https://item.taobao.com/item.htm?id=522554271548) |
| 12   | SS8050         | Q1,Q2                                   | SOT-23(SOT-23-3)                   | 2    | [链接](https://item.taobao.com/item.htm?id=522573790562) |
| 13   | 10k            | R1,R2,R4,R5,R14,R15,R17                 | R0603                              | 7    | [链接](https://item.taobao.com/item.htm?id=525777943950) |
| 14   | 100k           | R10                                     | R0603                              | 1    |                                                  |
| 15   | 1k             | R3,R6,R11,R12,R13                       | R0603                              | 5    |                                                  |
| 16   | 4K7            | R7,R8,R9                                | R0603                              | 3    |                                                  |
| 17   | 1k             | RN1,RN2                                 | RES-ARRAY-SMD_0603-8P-L3.2-W1.6-BL | 2    | [链接](https://item.taobao.com/item.htm?id=522554151720) |
| 18   | RT9013-33GB    | U1,U3                                   | SOT-23-5_L3.0-W1.7-P0.95-LS2.8-BR  | 2    | [链接](https://item.taobao.com/item.htm?id=546701607083) |
| 19   | W25Q128JVSIQTR | U10                                     | SOIC-8_L5.3-W5.3-P1.27-LS8.0-BL    | 1    | [链接](https://item.taobao.com/item.htm?id=564591570051) |
| 20   | STM32F103C8T6  | U2                                      | LQFP-48_L7.0-W7.0-P0.50-LS9.0-BL   | 1    |                                                  |
| 21   | BMP280         | U4                                      | SENSOR-TH_BMP280-BL                | 1    | [链接](https://item.taobao.com/item.htm?id=536588151627) |
| 22   | MPU6050        | U5                                      | QFN50P400X400X95-25T265X275N       | 1    | [链接](https://item.taobao.com/item.htm?id=522575310310) |
| 23   | CP2104-F03-GM  | U6                                      | QFN-24_L4.0-W4.0-P0.50-BL-EP2.7    | 1    | [链接](https://item.taobao.com/item.htm?id=552233398369) |
| 24   | SH1.0A-4P 卧贴 | U7                                      | CONN-SMD_SH1.0A-4P-WT              | 1    | [链接](https://item.taobao.com/item.htm?id=565715285795) |
| 25   | TYPE-C-16P     | USB1                                    | USB-C-SMD_TYPE-C16PIN              | 1    | [链接](https://item.taobao.com/item.htm?id=573090887123) |
| 26   | 8MHz           | X1                                      | OSC-SMD_3P-L3.2-W1.3-P1.2-L        | 1    | [链接](https://item.taobao.com/item.htm?id=620069296989) |